from RakosignsAPI import RakosignsAPI
import math
import BigWorld
import Keys
from Avatar import PlayerAvatar
from gui import InputHandler

def inject_handle_key_event(event):
    global model_list, isModelCreated, CreatingMode, DelMode, positions_path, events, models_path
    if event.key == Keys.KEY_P and event.isKeyDown() and not event.isShiftDown():
        if not isModelCreated:
            model_list = API.create_models(positions_path, model_list)
        else:
            model_list = API.delete_models(model_list)
        isModelCreated = not isModelCreated
    if DelMode:
        if event.key == Keys.KEY_DELETE and event.isKeyDown():
            real_pos=BigWorld.player().vehicle.position
            data_json = API.read_config(positions_path)
            map_name = BigWorld.player().arena.arenaType.geometryName
            dist=0
            k=0
            i=0
            if map_name in data_json:
                for modelConf in data_json[map_name]:
                    dist2 = math.sqrt((modelConf['position'][0]-real_pos[0])**2+(modelConf['position'][1]-real_pos[1])**2+(modelConf['position'][2]-real_pos[2])**2)
                    if dist==0 or dist>dist2:
                        dist=dist2
                        k=i
                    i+=1
                if data_json[map_name]is not None:
                    del data_json[map_name][k]
                API.write_position_config(positions_path, data_json)
            model_list = API.delete_models(model_list)
            model_list = API.create_models(positions_path, model_list)
            isModelCreated = True
    if CreatingMode:
        if event.isKeyDown() and event.isShiftDown():
            for k in events:
                if event.key==eval('Keys.%s'%(k)):
                    model_name=models_path+events[k]
            global dat
            dat = API.read_config(positions_path)
            map_name = BigWorld.player().arena.arenaType.geometryName
            if map_name not in dat:
                dat[map_name] = []
            dat[map_name].append(
                {'model': model_name, 'position': map(lambda x: round(x, 4), BigWorld.player().vehicle.position)})
            API.write_position_config(positions_path, dat)
            model_list = API.delete_models(model_list)
            model_list = API.create_models(positions_path, model_list)
            isModelCreated = True
            
def hook_on_entering_world(self, prereqs):
    origin_onEnterWorld(self, prereqs)
    InputHandler.g_instance.onKeyDown += inject_handle_key_event

def hook_on_leaving_world(self):
    origin_onLeaveWorld(self)
    InputHandler.g_instance.onKeyDown -= inject_handle_key_event

API=RakosignsAPI()
isModelCreated = False
positions_path= 'res_mods/configs/pavel3333_signs/positions.json'
config_path='res_mods/configs/pavel3333_signs/rakosigns.json'
keys_path='res_mods/configs/pavel3333_signs/keys.json'

isON=True
CreatingMode = True
DelMode = True

model_list = []
config={}
keys={}

config=API.read_config(config_path)
isON=config['RakosignsON']
CreatingMode = config['Creating Mode']
DelMode = config['Delete Mode']
keys_config=API.read_config(keys_path)
models_path=keys_config['models_path']
activation_key=keys_config['activation_key']
events=keys_config['keys']

for key in events:
    keys[getattr(Keys, key)]=events[key]

if isON:
    print 'RAKOSIGNS: ON (author : Pavel3333, thx : Polyacov_Yury, spoter, StranikS_Scan, GPCracker, others'
    origin_onEnterWorld = PlayerAvatar.onEnterWorld
    PlayerAvatar.onEnterWorld = hook_on_entering_world
    origin_onLeaveWorld = PlayerAvatar.onLeaveWorld
    PlayerAvatar.onLeaveWorld = hook_on_leaving_world
