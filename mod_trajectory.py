from RakosignsAPI import RakosignsAPI
import math
import BigWorld
import Keys
from Avatar import PlayerAvatar
from gui import InputHandler

def inject_handle_key_event(event):
    global isModelCreated, CreatingMode, activation_key, creating_key, model_list, dat, positions_path,creating,model_name,real_pos
    if event.key == activation_key and event.isKeyDown() and not event.isShiftDown():
        print 1
        if not isModelCreated:
            model_list = API.create_models(positions_path, model_list)
        else:
            model_list = API.delete_models(model_list)
        isModelCreated = not isModelCreated
    if CreatingMode:
        if event.isKeyDown():
            if event.key==creating_key:
                creating=not creating
            if creating:
                dat = API.read_config(positions_path)
                map_name = BigWorld.player().arena.arenaType.geometryName
                if(real_pos==[]):
                    real_pos=BigWorld.player().vehicle.position
                if map_name not in dat:
                    dat[map_name] = []
                dat[map_name].append(
                    {'model': model_name, 'position': map(lambda x: round(x, 4), real_pos)})
                dist=math.sqrt((BigWorld.player().vehicle.position[0]-real_pos[0])**2+(BigWorld.player().vehicle.position[1]-real_pos[1])**2+(BigWorld.player().vehicle.position[2]-real_pos[2])**2)
                print 'dist=%s'%(dist)
                if(dist>=0.5):
                    print 100500
                    dat[map_name].append(
                        {'model': model_name, 'position': map(lambda x: round(x, 4), BigWorld.player().vehicle.position)})
                    API.write_position_config(positions_path, dat)
                    model_list = API.delete_models(model_list)
                    model_list = API.create_models(positions_path, model_list)
                    dist=0
                    real_pos=BigWorld.player().vehicle.position
            else:
                real_pos=[]
def hook_on_entering_world(self, prereqs):
    origin_onEnterWorld(self, prereqs)
    InputHandler.g_instance.onKeyDown += inject_handle_key_event

def hook_on_leaving_world(self):
    origin_onLeaveWorld(self)
    InputHandler.g_instance.onKeyDown -= inject_handle_key_event

API=RakosignsAPI()
isModelCreated = False
positions_path= 'res_mods/configs/pavel3333_trajectory/positions.json'
config_path='res_mods/configs/pavel3333_trajectory/trajectory.json'

isON=True
CreatingMode = True
creating=False

real_pos=[]
model_list = []
config={}

config=API.read_config(config_path)
isON=config['on']
CreatingMode = config['creating_mode']
activation_key=getattr(Keys, config['activation_key'])
creating_key=getattr(Keys, config['creating_key'])
model_name='objects/misc/bbox/sphere1.model'

if isON:
    print 'TRAJECTORY: ON (author : Pavel3333)'
    origin_onEnterWorld = PlayerAvatar.onEnterWorld
    PlayerAvatar.onEnterWorld = hook_on_entering_world
    origin_onLeaveWorld = PlayerAvatar.onLeaveWorld
    PlayerAvatar.onLeaveWorld = hook_on_leaving_world