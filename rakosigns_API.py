import codecs
import Math
import json
import BigWorld
import Keys
class RakosignsAPI(object):
    def __init__(self):
        pass
    def byte_ify(self, inputs):
        if inputs:
            if isinstance(inputs, dict):
                return {self.byte_ify(key): self.byte_ify(value) for key, value in inputs.iteritems()}
            elif isinstance(inputs, list):
                return [self.byte_ify(element) for element in inputs]
            elif isinstance(inputs, tuple):
                return tuple(self.byte_ify(element) for element in inputs)
            elif isinstance(inputs, unicode):
                return inputs.encode('utf-8')
            else:
                return inputs
        return inputs
    def create_models(self, path, model_list):
        data_json = self.read_config(path)
        mapName = BigWorld.player().arena.arenaType.geometryName
        if mapName in data_json:
            for modelConf in data_json[mapName]:
                matrix = Math.Matrix()
                matrix.setTranslate(tuple(modelConf['position']))
                model = BigWorld.Model(str(modelConf['model']))
                motor = BigWorld.Servo(matrix)
                model.addMotor(motor)
                BigWorld.addModel(model)
                model_list.append({'model': model, 'motor': motor})
        return model_list
    @staticmethod
    def delete_models(model_list):
        for i in range(len(model_list)):
            model_list[i]['model'].delMotor(model_list[i]['motor'])
            BigWorld.delModel(model_list[i]['model'])
            model_list[i]['model'], model_list[i]['motor'] = [None, None]
            model_list[i] = None
        model_list = []
        return model_list
    def read_config(self, path):
        with codecs.open(path, 'r', 'utf-8-sig') as my_file:
            data_json =  self.byte_ify(json.load(my_file))
            return data_json
            #except Exception:
            #print 'RAKOSIGNS: I can\'t read file \'%s\''%(path)
            #return {}
    @staticmethod
    def write_position_config(path, data):
        try:
            with codecs.open(path, 'w', encoding='utf-8-sig') as my_file:
                my_file.write(json.dumps(data, sort_keys=True, indent=4, ensure_ascii=False, encoding='utf-8-sig',
                                         separators=(',', ': ')))
        except Exception:
            print 'RAKOSIGNS: I can\'t write file \'%s\'!'%(path)
            return ''