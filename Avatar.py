import BigWorld
import Keys

import chapters

from Helpers import ChatConsole

class Avatar(BigWorld.Entity):

	def onEnterWorld(self, prereqs):
		# Set the position/movement filter to correspond to an avatar
		self.filter = BigWorld.AvatarFilter()

		# Load up the bipedgirl model
		self.model = BigWorld.Model( "characters/bipedgirl.model" )
	def say(self,msg):

		ChatConsole.ChatConsole.instance().write(
			"%d says: %s" % (self.id, msg) )


class PlayerAvatar(Avatar):

	def onEnterWorld(self,prereqs):

		Avatar.onEnterWorld(self,prereqs)

		# Set the position/movement filter to correspond to an player avatar
		self.filter = BigWorld.PlayerAvatarFilter()

		# Setup the physics for the Avatar
		self.physics = BigWorld.STANDARD_PHYSICS
		self.physics.velocityMouse = "Direction"
		self.physics.collide = True
		self.physics.fall = True
		self.isModel2Created = False
		self.model2 = BigWorld.Model("characters/bipedgirl.model")

	def handleKeyEvent(self,event):

		if event.isRepeatedEvent():
			return

		isDown = event.isKeyDown()

		# Get the current velocity
		v = self.physics.velocity
		# Update the velocity depending on the key input
		if event.key == Keys.KEY_W:
			v.z = isDown * 10.0
		elif event.key == Keys.KEY_S:
			v.z = isDown * -5.0
		elif event.key == Keys.KEY_A:
			v.x = isDown * -5.0
		elif event.key == Keys.KEY_D:
			v.x = isDown * 5.0
		elif event.key == Keys.KEY_P and event.isKeyDown:
			if(self.isModel2Created == False ):
				self.model2.addMotor(BigWorld.Servo(self.model.node('biped')));
				BigWorld.player().addModel(self.model2)
				self.isModel2Created=True;
			elif(self.isModel2Created == True ):
				self.model2.delMotor(BigWorld.Servo(self.model.node('biped')));
				BigWorld.player().delModel(self.model2)
				self.isModel2Created=False;
			self.model2.action("Spell")()

		# Save back the new velocity
		self.physics.velocity = v

# Avatar.py
